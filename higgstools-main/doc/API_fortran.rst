Interfacing HiggsTools with Fortran
===================================

HiggsTools does not provide a native Fortran interface. The C++ functions can, however, be
called from Fortran. An example program can be found at 

https://gitlab.com/thomas.biekoetter/fpmhtcaller