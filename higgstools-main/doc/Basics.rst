Enumerations, Constants and Errors
----------------------------------
HiggsPredictions also defined a number of utility types that appear as arguments
in the interface functions or are thrown as errors. These are documented in the
following.

Colliders and Experiments
=========================

.. doxygenenum:: Higgs::predictions::Collider

.. doxygenenum:: Higgs::predictions::ColliderType

.. doxygenenum:: Higgs::predictions::Experiment


Particle Quantum Numbers
========================

.. _CP:

.. doxygenenum:: Higgs::predictions::CP

.. _ECharge:

.. doxygenenum:: Higgs::predictions::ECharge

Mathematical and Physical Constants
===================================
.. doxygennamespace:: Higgs::predictions::constants

Errors
======

.. doxygenclass:: Higgs::predictions::InvalidInput

.. doxygenclass:: Higgs::predictions::InvalidChannel
