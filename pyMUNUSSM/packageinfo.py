import sys
import os

print(
""" S u p e r P y

    Finds regions of SUSY models' parameter spaces that are in
    best agreement with experimental data.
    Project: SuperPy.
    Author: Andrew Fowlie, KBFI, Tallinn.
    Version: 1.1
    Date: 05/14

	***** ADAPTATION TO MUNUSSM *****

    Package: superpy-munussm v.0.0000
    Author: DONALD KPATCHA
    Date: 2024
    Updated: subprograms
    New: more subprograms, tools, model, easier and better input formats, etc
    The modifications w.r.t the original SuperPy are listed in pyMUNUSSM/MODIFICATIONS
"""
)
